# Workshop React Native
16:00u - 21:00u @kantoor Navara

## BASIC KNOWLEDGE
Javascript  
MacBook

## PREWORK
1. Install Xcode  
    https://apps.apple.com/us/app/xcode/id497799835?mt=12
2. Install Homebrew  
    https://brew.sh/
3. Install Node  
	`brew install node`
4. Install Watchman  
	`brew install watchman`
5. Install OpenJDK  
	`brew tap AdoptOpenJDK/openjdk`  
    `brew cask install adoptopenjdk8`
6. Install React Native CLI  
    `npm install -g react-native-cli`
7. Create react native app  
    `npx react-native init pokemap`
9. Install dependencies in root  
    `cd pokemap`
    `yarn`
9. Install Pods  
    `npx pod-install`
10. Run iOS  
    `npx react-native run-ios`

## TRAINING
### #1 BottomTab and Stack navigation
- https://reactnavigation.org/docs/en/tab-based-navigation.html  
1. Install dependencies  
`yarn add @react-navigation/native`  
`yarn add react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view`  
`yarn add @react-navigation/bottom-tabs`  
2. Install pod 
3. gestire....
2. Create Overview.js and Map.js in screens folder  
3. Create AppNavigator.js in the navigation folder  
4. Make BottomTab with icons  
`yarn add react-native-vector-icons`  
tip: `Ionicons.loadFont();`  
     `'list-box', 'list', 'map'` are valid iconnames, with prefix of `'ios-'` for ios, or `'md-'` for android
- https://reactnavigation.org/docs/tab-based-navigation/#customizing-the-appearance
5. Load AppNavigator in App.js  
https://reactnavigation.org/docs/stack-navigator/  
6. Install dependencies  
`yarn add @react-navigation/stack`  
6. Create Details.js in the screens folder  
7. Create OverviewNavigator.js in the navigation folder  
9. Load OverviewNavigator in AppNavigator.js  

#### BONUS: Add badge component
- https://reactnavigation.org/docs/tab-based-navigation/#add-badges-to-icons

### #2 Overview page:
- https://medium.com/@alialhaddad/fetching-data-in-react-native-d92fb6876973
1. Create Pokemon.js in services folder  
2. fetch data from:  
https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json  
3. Use FlatList, and renderItem with id, img, name & type in Overview.js  
https://reactnative.dev/docs/flatlist  
4. Use inline style to style your overview page
5. Use AsyncStorage to store the data  
https://reactnative.dev/docs/asyncstorage

#### BONUS: Search functionality
- https://react-native-elements.github.io/react-native-elements/docs/searchbar.html

### #3.1 Detailview:
1. Use the TouchableOpacity to navigate to the details page, with navigation props
https://reactnavigation.org/docs/navigation-prop/
2. Use the navigation props to render the data

### #3.2 Styling:
1. Make index.js file in styles folder  
2. Create a stylesheet to style the screens  
https://reactnative.dev/docs/stylesheet

### #4 Maps:
1. Install react-native-maps  
https://github.com/react-native-community/react-native-maps  
`yarn add react-native-maps -E`
2. get Google API key for IOS
https://developers.google.com/maps/documentation/ios-sdk/get-api-key
3. Enable Google Maps by adding the two lines as below in ./ios/pokemon/AppDelegate.m  
```
+ #import <GoogleMaps/GoogleMaps.h>

@implementation AppDelegate
...

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
+  [GMSServices provideAPIKey:@"_YOUR_API_KEY_"]; // add this line using the api key obtained from Google Console
...
```
- https://github.com/react-native-community/react-native-maps/blob/master/docs/installation.md#enabling-google-maps-on-ios-react-native-all-versions  
4. Use the MapView in the map screen  
https://github.com/react-native-community/react-native-maps#mapview  
5. Get the pokemonLocation mock data  
https://bitbucket.org/Edwin_Caspers/rn-pokemon/raw/c17d3a36e79d67f8408beff471f5ed56476214ea/mock/pokemonLocations.json  
6. use the pokemonLocation data with the previous data of the pokemon, and render the image as marker  
tip: `image={{ uri: marker.img }}`  
https://github.com/react-native-community/react-native-maps#rendering-a-list-of-markers-on-a-map

#### BONUS: Get the current location
6. Install geolocation to get the current location
https://github.com/react-native-community/react-native-geolocation 
`yarn add @react-native-community/geolocation`
7. Set your location of the simulator  
    latitude: 52.0467481,  
    longitude: 5.2942639  
8. Use the current position to set the region of the map 
`Geolocation.getCurrentPosition...` 